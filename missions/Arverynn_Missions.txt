##BranchStart 0
veykoda_vareynn_branch = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = G35
		NOT = { map_setup = map_setup_random }
	}
	has_country_shield = yes
	
	##Mission Start
	pomentere_estates = {
		icon = mission_secure_khandesh
		required_missions {
			veykodan_rune_warriors
		}
		position = 2
		provinces_to_highlight = {
			OR = {
				province_id = 1140
				province_id = 2810
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			check_variable = {
					which = G35WarsWon
					value = 3
				}
			1140 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
			2810 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			1140 = {
				change_culture = veykodan
				add_province_modifier = {
					name = G35_pomentere_estates
					duration = -1
				}
			}
			2810 = {
				change_culture = veykodan
				add_province_modifier = {
					name = G35_pomentere_estates
					duration = -1
				}
			}
			add_accepted_culture = veykodan
			add_country_modifier = {
				name = G35_veykodan_rune_warriors
				duration = 3650 #10 Years
			}
		}
	}
	##Mission End
	##Mission Start
	buycev_policy = {
		icon = mission_unite_the_deccan
		required_missions {
			pomentere_estates
		}
		position = 3
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			num_of_owned_provinces_with = {
				value = 5
				owned_by = ROOT
				is_core = ROOT
			}
			num_of_subjects = 5
			any_hired_mercenary_company = {
				OR = {
					template = veykodan_guard_merc
					template = veykodan_guard_elite_merc
				}
				hired_for_months = 60
			}
		}
		effect = {
			country_event = { id = flavor_arverynn.1 } #Path of the Veykoda
			set_country_flag = G35_buycev_flag #Unlocks additional mercenary companies #TODO
		}
	}
	##Mission End
	##Mission Start
	veykodan_guard = {
		icon = mission_rb_unite_the_clans
		required_missions {
			buycev_policy
		}
		position = 4
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			mil_power = 200
			1139 = {
				has_manpower_building_trigger = yes
				has_forcelimit_building_trigger = yes
			}
			num_of_owned_provinces_with = {
				value = 4
				owned_by = ROOT
				is_core = ROOT
				culture = veykodan
				base_manpower = 4
			}
		}
		effect = {
			add_mil_power = -200
			random_hired_mercenary_company = {
				limit = {
					template = veykodan_guard_merc
				}
				add_company_manpower = 1
			}
			add_country_modifier = {
				name = G35_veykodan_rune_armour
				duration = -1
				
			}
			custom_tooltip = veykodan_rune_armour_tt #TODO, lose this when you disband the mercenaries
		}
	}
	##Mission End
	##Mission Start
	thromshana_rangers = {
		icon = mission_rb_unite_the_clans
		required_missions {
			veykodan_guard
		}
		position = 5
		provinces_to_highlight = {
			province_id = 1140
			NOT = {
				has_manpower_building_trigger = yes
				has_manufactory_trigger = yes
			}
		}
		trigger = {
			1140 = { #thromshana
				has_manpower_building_trigger = yes
				has_manufactory_trigger = yes
			}
			custom_trigger_tooltip = {
				tooltip = tromsolec_way_rebuilt_tt #TODO
				has_country_flag = G35_rebuilt_tromsolec_way
			}
			mil_tech = 9
		}
		effect = {
			1140 = { #thromshana
				add_province_modifier {
					name = G35_tromseloc_way
					duration = -1
				}
			}
			2781 = { #Tromseloc
				add_province_modifier {
					name = G35_tromseloc_way
					duration = -1
				}
			}
			add_country_modifier = {
				name = G35_thromshana_rangers
				duration = 7300 #20 Years
			}
		}
	}
	##Mission End
	##Mission Start
	along_the_tromseloc_way = {
		icon = mission_misty_forest
		required_missions {
			thromshana_rangers
		}
		position = 6
		provinces_to_highlight = {
			area = bosancovac_area 
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			bosancovac_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			1165 = {
				cede_province = ROOT
			}
		}
	}
	##Mission End
	##Mission Start
	the_blood_of_lukaus = {
		icon = mission_marathi_fiefs
		required_missions {
			along_the_tromseloc_way
		}
		position = 7
		provinces_to_highlight = {
			OR = {
				area = trojvare_area
				area = brelar_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			trojvare_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			brelar_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			add_authority = 10
			1171 = {
				cede_province = ROOT
			}
		}
	}
	##Mission End
	##Mission Start
	bulwark_of_the_east = {
		icon = mission_magnificent_castle
		required_missions {
			the_blood_of_lukaus
		}
		position = 8
		provinces_to_highlight = {
			area = amacimst_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			amacimst_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			1172 = {
				has_fort_building_trigger = yes
			}
			2742 = {
				has_fort_building_trigger = yes
			}
		}
		effect = {
			1172 = {
				add_province_modifier = {
					name = G35_bulwarks_of_the_east
					duration = -1
				}
			}
			2742 = {
				add_province_modifier = {
					name = G35_bulwarks_of_the_east
					duration = -1
				}
			}
		}
	}
	##Mission End
	##
	
}
##BranchEnd 0
##BranchStart 1
conquest_sarda_middle = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = G35
		NOT = { map_setup = map_setup_random }
	}
	has_country_shield = yes
	
	##Mission Start
	veykodan_rune_warriors = {
		icon = mission_mayan_conquest
		required_missions {
			
		}
		position = 1
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			mil_power = 50
			treasury = 50
		}
		effect = {
			add_mil_power = -50
			add_treasury = -50
			set_country_flag = G35_veykodan_mercs_available_flag
			add_country_modifier = {
				name = G35_veykodan_rune_warriors
				duration = 1825 #5 Years
			}
			custom_tooltip = G35_dont_lose_veykodans_tt
		}
	}
	##Mission End
	##Mission Start
	seize_the_imperial_palace = {
		icon = mission_sovereign_principality
		required_missions {
			defeat_the_rebellion
		}
		position = 3
		provinces_to_highlight = {
			OR = {
				owned_by = G86
				area = hradapolere_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			hradapolere_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			OR = {
				G86 = {
					is_subject_of = G35
				}
				G86 = {
					exists = no
				}
			}
		}
		effect = {
			add_country_modifier = {
				name = G35_arverynn_resurgent
				duration = 3650 #10 Years
			}
			G86 = {
				define_heir = {
				dynasty = "yen Stantir"
				age = 28
				adm = 1
				dip = 3
				mil = 1
				claim = 50
				}
				kill_ruler = yes
				remove_historical_rival = G35
			}
		}
	}
	##Mission End
	##Mission Start
	guard_the_veykodan_approach = {
		icon = mission_sovereign_principality
		required_missions {
			seize_the_imperial_palace
		}
		position = 4
		provinces_to_highlight = {
			OR = {
				province_id = 1139
				province_id = 2858
				owned_by = G37
			}
			NOT = {
				has_fort_building_trigger = yes
			}
			
		}
		trigger = {
			G37 = {
				OR = {
					is_subject_of = ROOT
					NOT = {
						exists = yes
					}
				}
			}
			2858 = {
				has_fort_building_trigger = yes
				country_or_non_sovereign_subject_holds = ROOT
			}
			1139 = {
				has_fort_building_trigger = yes
				owned_by = ROOT
			}
		}
		effect = {
			add_country_modifier = {
				name = G35_stable_veykoda_frontier
				duration = 7300 #20 Years
			}
		}
	}
	##Mission End
	##Mission Start
	unity_of_the_sarda = {
		icon = secure_our_rule
		required_missions {
			veykodan_guard guard_the_veykodan_approach rightful_rulers_of_sarda
		}
		position = 5
		provinces_to_highlight = {
			region = sarda_region
			culture = sarda
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			sarda_region = {
				type = all
				OR = {
					AND = {
						culture = sarda
						country_or_non_sovereign_subject_holds = ROOT
					}
					is_city = no
					NOT = {
						culture = sarda
					}
				}
			}
		}
		effect = {
			every_known_country = { #TODO, custom tooltip
				limit = {
					primary_culture = sarda
					is_subject_of = G35
				}
				add_country_modifier = {
					name = G35_sarda_loyalty
					duration = -1
				}
			}
		}
	}
	##Mission End
	##Mission Start
	march_on_mocvamsto = {
		icon = secure_our_rule
		required_missions {
			unity_of_the_sarda
		}
		position = 6
		provinces_to_highlight = {
			OR = {
				area = arganjuzorn_area
				area = mocvare_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			mocvare_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			arganjuzorn_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			1186 = {
				cede_province = ROOT
			}
			add_prestige = 25
		}
	}
	##Mission End
	##Mission Start
	vanquich_the_battlekings = {
		icon = secure_our_rule
		required_missions {
			march_on_mocvamsto
		}
		position = 7
		provinces_to_highlight = {
			OR = {
				area = malacnar_area
				area = drevkenuc_area
				area = uslad_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			uslad_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			drevkenuc_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			malacnar_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			add_prestige = 25
			add_country_modifier = {
				name = G35_vanquished_battlekings
				duration = 7300 #20 Years
			}
		}
	}
	##Mission End
	##Mission Start
	knights_of_the_bush = {
		icon = secure_our_rule
		required_missions {
			vanquich_the_battlekings
		}
		position = 8
		provinces_to_highlight = {
			area = grebniesth_area
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			grebniesth_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			add_country_modifier = {
				name = G35_knights_of_the_bush
				duration = 7300 #20 Years
			}
		}
	}
	##Mission End
	##Mission Start
	surge_beyond_the_dark_ages = {
		icon = mission_expand_inland
		required_missions {
			bulwark_of_the_east knights_of_the_bush towards_the_high_city
		}
		position = 9
		provinces_to_highlight = {
			superregion = ynn_superregion
			OR = {
				culture = rzentur
				culture = dolindhan
				culture = sarda
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			ynn_superregion = {
				type = all
				OR = {
					culture = rzentur
					culture = dolindhan
					culture = sarda
				}
				country_or_non_sovereign_subject_holds = ROOT
			}
			stability = 2
			legitimacy = 90
			NOT = {
				primitives = yes
			}
		}
		effect = {
			add_country_modifier = {
				name = G35_surge_beyond_dark_ages
				duration = 18250 #50 Years
			}
		}
	}
	##Mission End
	##
	
}
##BranchEnd 1
##BranchStart 2
rightful_rulers_branch = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = G35
		NOT = { map_setup = map_setup_random }
	}
	has_country_shield = yes
	
	##Mission Start
	a_true_vyrekynn = {
		icon = mission_empire
		required_missions {
			
		}
		position = 1
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			NOT = {
				has_ruler = "Calrodiy IV"
			}
		}
		effect = {
			add_legitimacy = 10
		}
	}
	##Mission End
	##Mission Start
	defeat_the_rebellion = {
		icon = mission_subdue_rajputana
		required_missions {
			veykodan_rune_warriors a_true_vyrekynn resolve_our_debts
		}
		position = 2
		provinces_to_highlight = {
			OR = {
				province_id = 2810
				province_id = 1148
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			2810 = {
				owned_by = ROOT
			}
			OR = {
				1148 = {
					owned_by = ROOT
				}
				AND = {
					1148 = {
						owned_by = U24
					}
					U24 = {
						is_subject_of = ROOT
					}
				}
			}
			G36 = {
				OR = {
					NOT = {
						has_ruler = "Alaran I"
					}
					exists = no
				}
			}
		}
		effect = {
			add_prestige = 15
			add_mil_power = 50
		}
	}
	##Mission End
	##Mission Start
	teach_them_a_lesson = {
		icon = mission_expand_inland
		required_missions {
			defeat_the_rebellion
		}
		position = 3
		provinces_to_highlight = {
			OR = {
				province_id = 2859
				owned_by = G36
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			G36 = {
				OR = {
					exists = no
					is_subject_of = G35
				}
			}
			2859 = {
				OR = {
					owned_by = G35
					owned_by = G36
				}
			}
		}
		effect = {
			G36 = {
				every_owned_province = {
					cede_province = G35
				}
			}
			country_event = { id = flavor_arverynn.3 } #Burning the tree of trompolere
		}
	}
	##Mission End
	##Mission Start
	rightful_rulers_of_sarda = {
		icon = mission_scepter_of_scepters
		required_missions {
			teach_them_a_lesson
		}
		position = 4
		provinces_to_highlight = {
			is_capital_of = G39
		}
		trigger = {
			legitimacy = 95
			num_of_subjects = 5
			G39 = {	
				has_opinion = {
				who = ROOT
				value = 125
				}
			}
		}
		effect = {
			add_authority = 50
			G35 = {
				create_subject = {
					subject_type = ynnic_iosahar
					subject = G39
				}
			}
		}
	}
	##Mission End
	##Mission Start
	arverynnic_rennaisance = {
		icon = sponsor_the_arts
		required_missions {
			rightful_rulers_of_sarda currency_reform
		}
		position = 5
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			OR = {
				AND = {
					artist = 2
					employed_advisor = {
						culture = sarda
						type = artist
					}
				}
				AND = {
					prestige = 50
					adm_power = 100
				}
				adm_power = 200
			}
			treasury = 350
			has_estate_privilege = estate_burghers_patronage_of_the_arts	
		}
		effect = {
			if = {
				limit = {
					artist = 2
					employed_advisor = {
						culture = sarda
						type = artist
					}
				}
			}
			else_if = {
				limit = {
					prestige = 50
					adm_power = 100
				}
				add_adm_power = -100
			}
			else = {
				add_adm_power = -200
			}
			add_treasury = -350
			1139 = {
				add_institution_embracement = {
					which = renaissance
					value = 50
				}
			}
		}
	}
	##Mission End
	##Mission Start
	relics_of_the_vyrekynn = {
		icon = mission_jawal_silver_mine
		required_missions {
			arverynnic_rennaisance
		}
		position = 6
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			prestige = 80
			treasury = 500
			adm_power = 50
			dip_power = 50
			mil_power = 50
		}
		effect = {
			country_event = { id = flavor_arverynn.100 } #Crafting a Relic of Vyrekynn chain
		}
	}
	##Mission End
	##Mission Start
	subjugate_the_rzentur = {
		icon = mission_invade_maharashtra
		required_missions {
			march_on_mocvamsto
		}
		position = 7
		provinces_to_highlight = {
			OR = {
				area = pomvasonn_area
				area = juzondezan_area
				area = aivndezan_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			pomvasonn_area = {
				type = all
				OR = {
					is_city = no
					country_or_non_sovereign_subject_holds = ROOT
				}
			}
			juzondezan_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			aivndezan_area = {
				type = all
				OR = {
					is_city = no
					country_or_non_sovereign_subject_holds = ROOT
				}
			}
		}
		effect = {
			add_country_modifier = {
				name = G35_defeated_the_dragon_cult
				duration = 9125 #25 Years
			}
		}
	}
	##Mission End
	##Mission Start
	towards_the_high_city = {
		icon = mission_fortifying_the_plain
		required_missions {
			subjugate_the_rzentur
		}
		position = 8
		provinces_to_highlight = {
			OR = {
				area = svemel_area
				area = munapeim_area
				area = drozmajaivn_area
				area = zasich_area
			}
			NOT = {
				is_city = no
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			svemel_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}	
			munapeim_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT

			}
			drozmajaivn_area = {
				type = all
				OR = {
					is_city = no
					country_or_non_sovereign_subject_holds = ROOT
				}
			}
			zasich_area = {
				type = all
				OR = {
					is_city = no
					country_or_non_sovereign_subject_holds = ROOT
				}
			}
		}
		effect = {
			add_authority = 25
			if = {
				limit = {
					religion = ynn_river_reformed
				}
				add_country_modifier = {
				name = G35_victory_over_dragon_cult
				duration = 9125 #25 Years
				}
			}
			else = {
				add_prestige = 25
			}
			
		}
	}
	##Mission End
	##
	
}
##BranchEnd 2
##BranchStart 3
reform_branch = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = G35
		NOT = { map_setup = map_setup_random }
	}
	has_country_shield = yes
	
	##Mission Start
	resolve_our_debts = {
		icon = mission_vedic_education
		required_missions {
			
		}
		position = 1
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			NOT = {
				num_of_loans = 1
			}
			is_year = 1446
		}
		effect = {
			add_country_modifier = {
				name = G35_cautious_economics
				duration = 7300 #20 Years
			}
		}
	}
	##Mission End
	##Mission Start
	centralized_bureaucracy = {
		icon = mission_writing_book
		required_missions {
			defeat_the_rebellion
		}
		position = 3
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			crown_land_share = 40
		}
		effect = {
			add_country_modifier = {
				name = G35_arverynnic_administration #Gets turned into a reform when you reform religion
				duration = -1
			}
		}
	}
	##Mission End
	##Mission Start
	currency_reform = {
		icon = reform_the_state
		required_missions {
			centralized_bureaucracy
		}
		position = 4
		provinces_to_highlight = {
			province_id = 1134
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			1134 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
			authority = 10
			adm_power = 100
		}
		effect = {
			add_authority = -10
			add_adm_power = -100
			add_years_of_income = 3
			1134 = {
				if = {
					limit = {
						1134 = {
							NOT = {
								owned_by = ROOT
							}
						}
					}
				cede_province = ROOT
				}
				add_province_modifier = {
					name = G35_imperial_mint
					duration = -1
				}
			}
		}
	}
	##Mission End
	##Mission Start
	land_reform = {
		icon = mission_rice_field
		required_missions {
			currency_reform
		}
		position = 5
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			num_of_owned_provinces_with = {
				value = 10
				is_core = ROOT
			}
			adm_power = 100
			mil_power = 100
			crown_land_share = 50
		}
		effect = { #TODO
			change_estate_land_share = {
				estate = estate_nobles
				share = -10
			}
			give_estate_land_share_gigantic = { estate = all }
			change_estate_land_share = {
				estate = estate_burghers
				share = 20
			}
			add_country_modifier = {
				name = G35_arverynnic_feudalism
				duration = -1
			}
		}
	}
	##Mission End
	##Mission Start
	vyrisamsto = {
		icon = take_constantinople
		required_missions {
			arverynnic_rennaisance land_reform sarda_stability
		}
		position = 6
		provinces_to_highlight = {
			province_id = 1139
		}
		trigger = {
			prestige = 80
			1139 = {
				development = 35
				has_tax_building_trigger = yes
				has_production_building_trigger = yes
				has_manpower_building_trigger = yes
			}
		}
		effect = {
			1139 = {
				add_center_of_trade_level = 1
				remove_province_modifier = ynn_urban_decline
				add_province_modifier = {
					name = G35_rapid_urbanization
					duration = 7300 #20 Years
				}
			}
		}
	}
	##Mission End
	##Mission Start
	foreigners_at_the_gates = {
		icon = the_italian_league
		required_missions {
			vyrisamsto
		}
		position = 7
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			any_known_country = {
				OR = {
					technology_group = tech_cannorian
					technology_group = tech_elven
					technology_group = tech_dwarven
					technology_group = tech_halfling
				}
			}
			1139 = {
				owned_by = ROOT
				is_core = ROOT
			}
		}
		effect = {
			country_event = { id = flavor_arverynn.50 } #Foreigners at the gates
		}
	}
	##Mission End
	##Mission Start
	pansvovych = {
		icon = personal_diplomacy
		required_missions {
			foreigners_at_the_gates
		}
		position = 8
		provinces_to_highlight = {
			OR = {
				is_capital_of = H35
				is_capital_of = H58
				is_capital_of = H59
				is_capital_of = H54
				is_capital_of = H55
				is_capital_of = H56
				is_capital_of = H57
				is_capital_of = G94
			}
		}
		trigger = {
			H35 = {
				is_subject_of = ROOT
			}
			OR = {
				H58 = {
					is_subject_of = ROOT
				}
				H59 = {
					is_subject_of = ROOT
				}
			}
			OR = {
				H54 = {
					is_subject_of = ROOT
				}
				H55 = {
					is_subject_of = ROOT
				}
			}
			OR = {
				H56 = {
					is_subject_of = ROOT
				}
				H57 = {
					is_subject_of = ROOT
				}
			}
			G94 = {
				is_subject_of = ROOT
			}
		}
		effect = {
			add_country_modifier = { #Add event doubling this at 10 cannorian subjects
				name = G35_lord_of_lords
				duration = -1
			}
		}
	}
	##Mission End
	##Mission Start
	dominate_the_tributaries = {
		icon = mission_pow_powhatan_fleet
		required_missions {
			pansvovych
		}
		position = 9
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			1139 = { #Lower Ynn tradenode
				trade_share = {
					country = ROOT
					share = 80
				}
				has_trade_building_trigger = yes
			}
			num_of_subjects = 8
			num_of_owned_provinces_with = {
				value = 20
				is_core = ROOT
			}
		}
		effect = {
			add_mercantilism = 5
			add_country_modifier = {
				name = G35_veykodan_tributaries
				duration = 18250 #50 Years
			}
		}
	}
	##Mission End
	##
	
}
##BranchEnd 3
##BranchStart 4
economy_branch = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = G35
		NOT = { map_setup = map_setup_random }
	}
	has_country_shield = yes
	
	##Mission Start
	emergency_measures = {
		icon = mission_high_income
		required_missions {
			
		}
		position = 1
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			num_of_loans = 10
			NOT = {
				is_year = 1446
			}
		}
		effect = {
			add_treasury = 200
			add_corruption = 5
			add_country_modifier = {
				name = G35_taxed_arverynn_citizens
				duration = 1825
			}
			custom_tooltip = G35_promise_of_reclamation_tt
			hidden_effect = {
				set_country_flag = G35_taxed_citizens_flag
			}
		}
	}
	##Mission End
	##Mission Start
	rebuild_our_city = {
		icon = mission_conquer_novgorod
		required_missions {
			resolve_our_debts
		}
		position = 2
		provinces_to_highlight = {
			province_id = 1139
			NOT = {
				has_tax_building_trigger = yes
				has_production_building_trigger = yes
				development = 20
			}
		}
		trigger = {
			1139 = {
				has_tax_building_trigger = yes
				has_production_building_trigger = yes
				development = 20
			}
		}
		effect = {
			1139 = {
				add_province_modifier = {
					name = G35_capital_growth 
					duration = 7300 #20 Years
				}
			}
			arverynn_area = {
				limit = {
					owned_by = G35
					}
					add_prosperity = 25
			}
		}
	}
	##Mission End
	##Mission Start
	the_northern_connection = {
		icon = mission_conquer_malwa_bah
		required_missions {
			rebuild_our_city
		}
		position = 3
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			G85 = {	
				has_opinion = {
				who = ROOT
				value = 200
				}
				is_subject_of = ROOT
			}
			1140 = {
				has_trade_building_trigger = yes
			}
		}
		effect = {
			G35 = {
				inherit = G85
			}
			custom_tooltip = G35_tromseloc_way_tt
			hidden_effect = {
				set_country_flag = G35_tromseloc_way_flag #Unlocks Decision to colonize Tromseloc Way
			}
		}
	}
	##Mission End
	##Mission Start
	repopulate_the_countryside = {
		icon = prosperity_for_all
		required_missions {
			the_northern_connection
		}
		position = 4
		provinces_to_highlight = {
			OR = {
				area = arverynn_area
				area = hradapolere_area
				area = ynncytr_area
			}
			NOT = {
				development = 10
				has_production_building_trigger = yes
				has_manpower_building_trigger = yes
			}
			owned_by = ROOT
		}
		trigger = {
			num_of_owned_provinces_with = {
				value = 5
				NOT = {
					province_id = 1139
				}
				OR = {
					area = arverynn_area
					area = hradapolere_area
					area = ynncytr_area
				}
				development = 10
				has_production_building_trigger = yes
				has_manpower_building_trigger = yes
			}
		}
		effect = {
			1139 = {
				add_base_production = 1
				add_base_tax = 1
				add_base_manpower = 1
			}
			add_yearly_manpower = 0.5
		}
	}
	##Mission End
	##Mission Start
	sarda_stability = {
		icon = polish_diplomacy
		required_missions {
			repopulate_the_countryside
		}
		position = 5
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			stability = 2
			arverynn_area = {
				type = all
				owned_by = ROOT
				is_core = ROOT
			}
		}
		effect = {
			add_country_modifier = {
				name = G35_sarda_stability
				duration = 7300 #20 Years
			}
		}
	}
	##Mission End
	##Mission Start
	the_teal_keep = {
		icon = mission_central_asian_city
		required_missions {
			vyrisamsto
		}
		position = 7
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			has_great_project = {
				type = teal_keep
				tier = 2
			}
			1139 = {
				has_fort_building_trigger = yes
				base_manpower = 8
			}
		}
		effect = {
			define_advisor = {
				type = statesman
				culture = sarda
				discount = yes
				skill = 3
			}
		}
	}
	##Mission End
	##Mission Start
	cannorian_encroachment = {
		icon = mission_rb_colonise_canada
		required_missions {
			foreigners_at_the_gates
		}
		position = 8
		provinces_to_highlight = {
			##PROVINCESHIGHLIGHTCODE
		}
		trigger = {
			custom_trigger_tooltip = {
				tooltip = G35_empty_tooltip
				has_country_flag = G35_veykodan_mercs_available_flag
			}
			num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
				value = 10
				NOT = {
					owned_by = ROOT
				}
				region = veykoda_region
				owner = {
					NOT = {
						technology_group = tech_ynnic
					}
				}
			}
			num_of_owned_provinces_with = {
				value = 4
				culture = veykodan
			}
			veykoda_region = {
				type = all
				any_province = {
					is_city = no
				}
			}
		}
		effect = {
			custom_tooltip = G35_veykodan_migration_tt
			hidden_effect = {
				every_owned_province = {
					limit = {
						culture = veykodan
					}
					add_base_tax = -1
					add_base_manpower = -1
					change_culture = sarda
				}
			}
			if = {
				limit = {
					any_province = {
						OR = {
							area = ynncytr_area
							area = favesrinn_area
							area = adbrabohvi_area
						}
						is_city = no
					}
				}
				random_province = {
					limit = {
						OR = {
								area = ynncytr_area
								area = favesrinn_area
								area = adbrabohvi_area
							}
						is_city = no
					}
					create_colony = 1000
					add_core = U21
					cede_province = U21
					add_base_manpower = 4
					add_base_tax = 4
				}
			}
			else_if = {
				random_province = {
					limit = {
						region = veykoda_region
						is_city = no
					}
					create_colony = 1000
					add_core = U21
					cede_province = U21
					add_base_manpower = 4
					add_base_tax = 4
				}
			}
			ROOT = {
				create_subject = {
					subject_type = march
					subject = U21
				}
			}
			U21 = {
				add_country_modifier = {
					name = G35_veykodan_migration
					duration = 7300 #20 Years
				}
			}
		}
	}
	##Mission End
	##Mission Start
	radavezyr = {
		icon = mission_rb_colonise_canada
		required_missions {
			cannorian_encroachment
		}
		position = 9
		provinces_to_highlight = {
				
		}
		trigger = {
			U21 = {
				num_of_owned_provinces_with = {
					value = 5
					is_core = U21
					NOT = {
						culture = sarda
					}
				}
			}
		}
		effect = {
			U21 = {
				add_country_modifier = {
					name = G35_protected_by_arverynn
					duration = -1
				}
			}
			set_country_flag = G35_improved_guard_flag 
		}
	}
	##Mission End
	##
	
}
##BranchEnd 4
